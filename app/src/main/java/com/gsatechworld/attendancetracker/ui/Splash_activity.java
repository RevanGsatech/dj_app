package com.gsatechworld.attendancetracker.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsatechworld.attendancetracker.R;
import com.gsatechworld.attendancetracker.ui.login.Login_screen;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Splash_activity extends AppCompatActivity {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.line_bar)
    LinearLayout line_bar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        ButterKnife.bind(this);

        title.startAnimation(AnimationUtils.loadAnimation(Splash_activity.this, R.anim.left_to_right));
        line_bar.startAnimation(AnimationUtils.loadAnimation(Splash_activity.this, R.anim.right_to_left));



        int secondsDelayed = 1;
        new Handler().postDelayed(() -> {

                startActivity(new Intent(Splash_activity.this, Login_screen.class));
                finish();

        }, secondsDelayed * 2000);


    }
}

