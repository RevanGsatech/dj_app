package com.gsatechworld.attendancetracker.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.gsatechworld.attendancetracker.R;
import com.gsatechworld.attendancetracker.base.BaseActivity;

public class Login_screen extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
    }
}
