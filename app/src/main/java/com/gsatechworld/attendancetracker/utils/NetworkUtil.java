package com.gsatechworld.attendancetracker.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;


//NetworkUtil
public class NetworkUtil {


    public static String NO_INTERNET = "No Internet, Please try again";
    public static String INTERNET_BACK = "Internet Back";
    public static String SERVVER_ERROR = "Sorry! Server Error";
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    public static NetworkUtil instance;
    private Context _context;

    public NetworkUtil(Context context) {
        this._context = context;
    }

    public static NetworkUtil getInstance(Context _context) {
        if (instance == null) {
            instance = new NetworkUtil(_context);
        }
        return instance;
    }

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static Boolean getConnectivityStatusString(Context context) {
        int conn = NetworkUtil.getConnectivityStatus(context);
        Boolean statusFlag = false;
        //String status = null;
        if (conn == NetworkUtil.TYPE_WIFI) {
//            status = "Wifi enabled";
            statusFlag = true;
        } else if (conn == NetworkUtil.TYPE_MOBILE) {
//            status = "Mobile data enabled";
            statusFlag = true;
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
//            status = "Not connected to Internet";
            statusFlag = false;
        }
        return statusFlag;
    }

    public static int getConnectivityStatusWithNetworkStrength(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public boolean isConnectingToInternet() {
        if (_context != null) {
            ConnectivityManager connMgr = (ConnectivityManager) _context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

            return networkInfo != null && networkInfo.isConnected();
        }

        return false;
    }

    public int getWifiSpeed(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        int linkSpeed = wifiManager.getConnectionInfo().getRssi();
        return linkSpeed;
    }


}
