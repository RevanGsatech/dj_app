/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.gsatechworld.attendancetracker.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.gsatechworld.attendancetracker.utils.CommonUtils.EMPTY_DASH_STRING;


public abstract class BaseActivity extends AppCompatActivity {


    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public void showProgressDialog() {
       /* dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.progress_loading_layout);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();*/
    }


    public void dismissProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.cancel();
            dialog = null;
        }
    }

    public void setStringToText(TextView textView, String stringValue) {
        if (!TextUtils.isEmpty(stringValue)) {
            textView.setText(stringValue);
        } else {
            textView.setText(EMPTY_DASH_STRING);
        }
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);


        }
    }

    /**
     * private void showSnackBar(String message) {
     * Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
     * message, Snackbar.LENGTH_SHORT);
     * View sbView = snackbar.getView();
     * TextView textView = sbView
     * .findViewById(android.support.design.R.id.snackbar_text);
     * textView.setTextColor(ContextCompat.getColor(this, R.color.white));
     * snackbar.show();
     * }
     * <p>
     * <p>
     * public void onError(String message) {
     * if (message != null) {
     * showSnackBar(message);
     * }  // showSnackBar(getString(R.string.some_error));
     * <p>
     * }
     */


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * This method is is used to a image file in user's mobile local storage
     *
     * @return - a jpg image file
     */
    public File createPhotoFile() {
        String fileName = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        File imageFile = null;
        try {
            imageFile = File.createTempFile(fileName, ".jpg",
                    getExternalFilesDir(Environment.DIRECTORY_PICTURES));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageFile;
    }
}